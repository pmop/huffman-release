import QtQuick 2.0

Rectangle {
    //model is a list of {"name":"somename", "url":"file:///some/url/mainfile.qml"}
    //function used to add to model A) to enforce scheme B) to allow Qt.resolveUrl in url assignments

    color: "#eee"
    function addExample(name, desc, url)
    {
        myModel.append({"name":name, "description":desc, "url":url})
    }
    function hideExample()
    {
        ei.visible = false;
    }
    function remove(dex){
        myModel.remove(dex)
    }

    ListView {
        clip: true
        delegate: SimpleLauncherDelegate {
            id: mDelegate
            exampleItem: ei
            onRemoveMe: {
                myModel.remove(mIndex)
            }
        }
        model: ListModel {id:myModel }
        anchors.fill: parent
    }
    Item {
        id: ei
        visible: false
        clip: true
        property url exampleUrl
        onExampleUrlChanged: visible = (exampleUrl == '' ? false : true); //Setting exampleUrl automatically shows example
        anchors.fill: parent
        anchors.bottomMargin: 40
        Rectangle {
            id: bg
            anchors.fill: parent
            color: "white"
        }
        MouseArea{
            anchors.fill: parent
            enabled: ei.visible
            //Eats mouse events
        }
    }
}

