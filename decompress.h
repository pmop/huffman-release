#ifndef DECOMPRESS_H
#define DECOMPRESS_H
#include <QDir>
#include "mtree.h"
#include "huffmandata.h"
#include "fileheaderdata.h"
class Decompress : public QObject
{
    Q_OBJECT
    FileHeaderData m_header;
    QFile* m_currentCompressedFile;
    HeaderStatus m_status = HeaderStatus::FAIL;
    quint32 m_huffmanPos;
    QString m_path;
    quint64 m_totalCompressedFileSize;
    HuffmanData m_data;
    FileStatus m_FileStatus;
    quint64 m_currentFileIterator;
    bool initial;
public:
    Decompress();
    void decompressFile(QString file,QString path = "./");
    ~Decompress();
signals:
    void sendBytes(QByteArray bytes,FileStatus status);
    void bitesChunk(QBitArray* bites, FileStatus status);
    void error (const QString);
private slots:
    void forwardErrors(const QString err);
public slots:
    void headerStatus(HeaderStatus status);
    void replyChunkRequest();
    void write(QByteArray bytes);
    void replyRequestOfBytes(quint64 atPosition);
};
#endif //END OF INCLUDE GUARD DECOMPRESS_H
