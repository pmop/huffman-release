import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
ApplicationWindow {
    //    flags: "FramelessWindowHint"
    id: mainWindow
    x: 40
    y: 40
    minimumWidth: 640
    minimumHeight: 220
    visible: true
    FileDialog {
        id: mFDialog
        selectMultiple: false
        selectFolder: false
        title: "Selecione um arquivo"
        onAccepted: {
            if ( FileUtils.fileName(mFDialog.fileUrl) )
            {
                filesList.addExample(FileUtils.fileName(mFDialog.fileUrl),FileUtils.pathToFile(mFDialog.fileUrl))
            }

        }
    }
    CFileDialog {
        id: cfDialog
    }

    toolBar: ToolBar {
        id: myToolBar
        RowLayout {
            anchors.fill: parent
            anchors.centerIn: parent
            Button {
                text: "Add..."
                onClicked: {
                    mFDialog.visible = true
                }
            }
        }
    }
    ColumnLayout {
        spacing: 2
        anchors.fill: parent
        Item {
            height: parent.height * 0.8
            width: parent.width
            Layout.fillHeight: true
            Layout.fillWidth: true
            FilesList {
                id: filesList
                anchors.fill: parent
            }
        }


    }
    statusBar: StatusBar {
        id: notify
        RowLayout {
            id: inRow
            Label {
                id: status
                text: ""
                font.pointSize: 6
                signal receiveText(string someText)
                signal receiveError(string err)
                onReceiveText: {
                    text = qsTr( "Success! Saved at:  " + someText )
                    console.log("Received text")
                }
                onReceiveError: {
                    text = qsTr( "Failure!  :" + err )
                }
                Component.onCompleted: {
                    FileUtils.savedAtChanged.connect(status.receiveText)
                    FileUtils.errorChanged.connect(status.receiveError)
                }
            }
        }
    }
}
