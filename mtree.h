#ifndef MTREE
#define MTREE
#include <string>
#include <sstream>
#include <QHash>
#include <QBitArray>
#include <QString>
#include <QDebug>
#include <QQueue>
namespace byteops {
QBitArray bitsFromBytes(const QByteArray data);
QByteArray bytesFromBits(const QBitArray data);
}
class MNode
{
    MNode* lc;
    MNode* rc;
    MNode* mParent;
    QString code;
    quint64 mWeight;
    int16_t mKey;//don't get back to uchar, we're using -1 to tell it's not a leaf
public:
    MNode(const quint64 weight,const int16_t key,MNode* l,MNode*r);
    MNode(const quint64 weight,const int16_t key);
    ~MNode();
    MNode* &getRc();
    MNode* &getLc();
    void setLc(MNode* &value);
    void setRc(MNode *value);
    quint64 weight() const;
    void setWeight(const quint64 weight);
    int16_t key() const;
    void setKey(const qint16 key);
    bool hasParent();
    void setParent(MNode* parent);
    void dbg(MNode* &node);
    void serializeTree(MNode* rootNode = nullptr, QByteArray *serialStream = nullptr, bool keyAsInt = false);
    void serializeTree(MNode *node = nullptr, QQueue<QBitArray> *stream = nullptr, quint64 *totalSize = nullptr);
    void generateCodeTable(MNode* node, QVector<QBitArray> &codes, quint64 *totalSize);
    bool isLeaf();
};
MNode* sum(MNode* node1,MNode* node2);
bool compNode(MNode* &a,MNode* &b);
#endif // MTREE
