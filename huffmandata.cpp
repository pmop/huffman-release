#include "huffmandata.h"
HuffmanData::HuffmanData() {
    m_fileStatus = FileStatus::NOTREADY;
    m_headerData = nullptr;
}
void HuffmanData::setup(FileHeaderData *header) {
    m_headerData = header;
    m_serializedTree = header->getTree ();
    m_tree = buildNode ();
    if (m_tree)
    {
        decode();
    } else {
        qDebug() << "Error: null tree (Possible invalid file)";
        emit error ( QStringLiteral("Possible invalid or corrupt file.") );
    }
}
HuffmanData::~HuffmanData() {
}
void HuffmanData::decode() {
    if ( m_fileStatus == FileStatus::NOTREADY )
    {
        // If we don't get answered, we'll treat it as a failure
        m_fileStatus = FileStatus::FAILURE;
        emit requestBitChunk ();
        decode ();
    }
    else if (m_fileStatus == FileStatus::FREADY)
    {
        m_decodedBytes.clear ();
        MNode* currentNode = m_tree;
        quint64 i = 0;
        quint64 currSize = m_bitChunk->size ();
        while ( i <= currSize ) {
            if ( currentNode->isLeaf () )
            {
                m_decodedBytes.append ( static_cast<quint8>(currentNode->key ()) );
                currentNode = m_tree; // reset to tree
            }
            if( m_bitChunk->at (i) ) // 1 -> right
            {
                if ( currentNode->getRc () )
                {
                    currentNode = currentNode->getRc ();
                } else {
                    qDebug() << "Invalid or corrupt data";
                    emit error ( QStringLiteral("Possible invalid or corrupt file.") );
                    return;
                }
            } else {
                if ( currentNode->getLc () )
                {
                    currentNode = currentNode->getLc ();
                } else {
                    qDebug() << "Invalid or corrupt data";
                    emit error ( QStringLiteral("Possible invalid or corrupt file.") );
                    return;
                }
            }
            i++;
        }
        delete m_bitChunk;
        emit decodedBytes (m_decodedBytes);
        m_decodedBytes.clear ();
        m_fileStatus = FileStatus::FAILURE;
        emit requestBitChunk ();
        decode();
    }
    else if (m_fileStatus == FileStatus::REACHEDEOF )
    {
        m_decodedBytes.clear ();
        MNode* currentNode = m_tree;
        quint64 i = 0;
        quint64 currSize = m_bitChunk->size () - m_headerData->padding ();
        while ( i <= currSize )
        {
            if ( currentNode->isLeaf () )
            {
                m_decodedBytes.append ( static_cast<quint8>(currentNode->key ()) );
                currentNode = m_tree; // reset to tree
            }
            if( m_bitChunk->at (i) ) // 1 -> right
            {
                if ( currentNode->getRc () != nullptr )
                {
                    currentNode = currentNode->getRc ();
                } else {
                    qDebug() << "Invalid or corrupt data";
                    return;
                }
            } else {
                if ( currentNode->getLc () != nullptr )
                {
                    currentNode = currentNode->getLc ();
                } else {
                    qDebug() << "Invalid or corrupt data";
                    return;
                }
            }
            i++;
        }
        delete m_bitChunk;
        emit decodedBytes (m_decodedBytes);
        m_decodedBytes.clear ();// i really hope it's position independent code
    }
    else if (m_fileStatus == FileStatus::FAILURE )
    {
        qDebug() << "Failure in requesting bytes to decode";
    }
}
void HuffmanData::dbg() {
    qDebug () << "\n\nHuffmanData dbg";
    if ( m_tree )
    {
        QByteArray serializedData;
        m_tree->serializeTree (m_tree,&serializedData,false);
    }
}

void HuffmanData::receiveBitChunk(QBitArray *chunk, FileStatus status)
{
    if (!chunk)
    {
        m_fileStatus = FileStatus::FAILURE;
    }
    m_bitChunk = chunk;
    m_fileStatus = status;
}
void HuffmanData::decodeTheseBytes(QByteArray bytes, FileStatus status)
{
    if ( status != FileStatus::FAILURE )
    {
        m_fileStatus = status;
    }
}
MNode *HuffmanData::buildNode() {
    if ( !m_serializedTree.isEmpty () ) {
        QBitArray value = m_serializedTree.dequeue ();
        if ( value.size () == 1 )
        {
            if ( value.at (0) ) // 1 -> true
            {
                value = m_serializedTree.dequeue ();
                QByteArray key = byteops::bytesFromBits (value);
                MNode* p = new MNode (0, quint8(key.at (0)) );
                if (!p) {
                    qDebug() << "Error (leaf)";
                    return nullptr;
                }
                return p;
            } else {
                MNode* l = buildNode();
                MNode* r = buildNode();
                MNode* parent = new MNode(0,-1,l,r);
                if (!parent) {
                    qDebug() << "Error (parent)";
                    return nullptr;
                }
                return parent;
            }
        } else {
            qDebug() << "Invalid or corrupt serialized data";
            return nullptr;
        }
    }
}

