#include "decompress.h"
/*
    TODO:
    Fix the append bug ->
    There is some bug with either appending bytes to file or reading
    chunks from it.
*/
Decompress::Decompress() {
    QObject::connect (&m_header,&FileHeaderData::headerStatus,
                      this, &Decompress::headerStatus);
    QObject::connect (&m_data,&HuffmanData::requestBitChunk,
                      this,&Decompress::replyChunkRequest);
    QObject::connect (&m_data,&HuffmanData::error,
                      this,&Decompress::forwardErrors);
    QObject::connect (this,&Decompress::bitesChunk,
                      &m_data,&HuffmanData::receiveBitChunk);
    QObject::connect (&m_data,&HuffmanData::decodedBytes,
                      this,&Decompress::write);
    initial = true;
    m_currentCompressedFile = nullptr;
    m_currentFileIterator = 0;
}
void Decompress::decompressFile(QString fileName, QString path)
{
    m_path = path;
    QFile file (fileName);
    if ( file.exists () )
    {
        if ( file.open (QIODevice::ReadOnly) )
        {
            m_header.readFromFile (file);
            m_currentCompressedFile = &file;//point to QFile so we can actually use it elsewhere
            m_totalCompressedFileSize = file.size ();
            switch (m_status) {
            case HeaderStatus::FAIL:
                qDebug() << "Failure in header";
                break;
            case HeaderStatus::READY:
                m_data.setup(&m_header);
                break;
            }
        }
    }
}
Decompress::~Decompress() {
}

void Decompress::forwardErrors(const QString err)
{
    emit error (err);
}
void Decompress::headerStatus(HeaderStatus status) {
    m_status = status;
}
void Decompress::write(QByteArray bytes)
{
    QDir dir(m_path);
    qDebug () << dir.absoluteFilePath(m_header.fileName());
    QFile file(dir.absoluteFilePath(m_header.fileName()));
    if ( file.exists () )
    {
        if (file.open ( QIODevice::Append) )//may or may not?
        {
            file.write (bytes);
        }  else {
            qDebug() << "Failed to append to file";
        }
    } else {
        if (file.open (QIODevice::WriteOnly) )
        {
            file.write (bytes);
        } else {
            qDebug() << "Failed to write to file";
        }
    }
}
void Decompress::replyRequestOfBytes(quint64 atPosition)
{
    m_currentCompressedFile->seek (atPosition);
    QByteArray bytes = m_currentCompressedFile->read ( std::pow(10,8) );//1MB
    if ( !(bytes.isNull () || bytes.isEmpty ()) )
    {
        qDebug() << "We're sending" << bytes.size () << "bytes";
        if ( m_currentCompressedFile->atEnd () ) {
            emit sendBytes (bytes,FileStatus::REACHEDEOF);
//            qDebug() << "There is no more to be read, EOF REACHED.";
        } else {
            emit sendBytes (bytes,FileStatus::FREADY);
//            qDebug() << "There is more to be read.";
        }
    } else {
        qDebug() << "Failure to decode file. Invalid file";
    }
}
void Decompress::replyChunkRequest()
{
    if (initial)
    {
        QBitArray* chunk;
        m_currentCompressedFile->reset ();
        m_currentCompressedFile->seek ( m_header.getHeaderEndPosition () - 1 );
        QByteArray lastbit = m_currentCompressedFile->read (1);
        QBitArray helper = byteops::bitsFromBytes (lastbit);// that last byte
        m_currentCompressedFile->seek ( m_header.getHeaderEndPosition () );
        QByteArray test = m_currentCompressedFile->read ( std::pow(10,8) );
        QBitArray test2 = byteops::bitsFromBytes (test);
        chunk = new QBitArray( test2.size () + m_header.getTreepadding (),false);// allocating for chunk
        for (qint32 i = 0; i < test2.size ();++i) {
            if (test2.at (i) )
            {
                chunk->setBit (i+m_header.getTreepadding (),true);
            }
        }
        quint8 offset = 8 - m_header.getTreepadding ();
        quint8 aux = 0;
        for (qint8 i = offset; i < 8 ; ++i) {
            if ( helper.at (i) )
            {
                chunk->setBit (aux,true);
            }
            aux++;
        }
        initial = false;//set initial off
        //tell if we still have more to be parsed
        m_currentCompressedFile->atEnd () ? m_FileStatus = FileStatus::REACHEDEOF : m_FileStatus = FileStatus::FREADY;
        //move iterator
        m_currentFileIterator += m_header.getHeaderEndPosition ();
        m_currentFileIterator += test.size ();
        if ( m_FileStatus == FileStatus::REACHEDEOF ) {
            m_currentCompressedFile->close (); // close file descriptor and point our pointer to null in case we finish reading file
            m_currentCompressedFile = nullptr;
        }
        emit bitesChunk (chunk,m_FileStatus);

    } else {
        m_currentCompressedFile->seek (m_currentFileIterator);
        QByteArray aux = m_currentCompressedFile->read ( std::pow(10,8) );
        QBitArray *chunk;
        chunk = new QBitArray(aux.size ()*8,false);
        *chunk = byteops::bitsFromBytes (aux);
        m_currentFileIterator += aux.size ();
        aux.clear ();
        m_currentCompressedFile->atEnd () ? m_FileStatus = FileStatus::REACHEDEOF : m_FileStatus = FileStatus::FREADY;
        if ( m_FileStatus == FileStatus::REACHEDEOF ) {
            m_currentCompressedFile->close (); // close file descriptor and point our pointer to null in case we finish reading file
            m_currentCompressedFile = nullptr;
        }
        emit bitesChunk (chunk,m_FileStatus);
    }
}
