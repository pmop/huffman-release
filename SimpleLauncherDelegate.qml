import QtQuick 2.0
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import QtQuick.Dialogs 1.2
Rectangle {
    id: container
    property Item exampleItem
    width: ListView.view.width
    height: button.implicitHeight + 22
    color: "#FBFBFB"
    signal removeMe(int mIndex)
    Item {
        id: exitButton
        width: 16
        height: 16
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 36
        Image {
            anchors.fill: parent
            source: "qrc:/trash.png"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                removeMe(index)
            }
        }
    }

    Rectangle {
        id: next
        width: 16
        height: width
        radius: width
        opacity: 0.7
        color:"transparent"
        border.width: 1
        border.color: "gray"
        Behavior on opacity {NumberAnimation {duration: 100}}
        Behavior on color {
            SequentialAnimation {
                ColorAnimation {
                    duration: 200
                }
                ColorAnimation {
                    to:"transparent"
                    duration: 200
                }
            }
        }
        //        source: "images/next.png"
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 16
        Rectangle {
            id: middleBal
            width: parent.width/2
            height: width
            radius: width
            color:"black"
            anchors.centerIn: parent
            Behavior on color {
                SequentialAnimation {
                    ColorAnimation {
                        to: "grey"
                        duration: 200
                    }
                    ColorAnimation {
                        to: "black"
                        duration: 200
                    }
                }
            }
        }
        FileDialog {
            id: cbuttonFileDialog
            selectExisting: false
            title: "Compress to..."
            onAccepted: {
                if ( decompressTo.isTriggered )
                {
                    decompressTo.urlReceived( cbuttonFileDialog.fileUrl)
                }
            }
        }
        CFileDialog {
            id: dialog
            onReceivedUrl: {
                compressTo.receivedUrl(mFileUrl)
            }
        }
        CFileDialog {
            id: decompressDialog
            selectFolder: true
            onReceivedUrl: {
                decompressTo.receivedUrl(mFileUrl)
            }
        }
        Menu {
            id: buttonMenu
            MenuItem {
                text: "Compress"
                onTriggered: {
                    FileUtils.compressFile( description )
                }
            }
            MenuItem {
                id: compressTo
                text: "Compress to..."
                property bool mTriggered: false
                signal receivedUrl(url myUrl)
                onTriggered: {
                    dialog.show()
                    mTriggered = true
                }
                onReceivedUrl: {
                    if ( mTriggered ) {
                        mTriggered = false
                        console.log("Compressing to:" + myUrl)
                        FileUtils.compressFileTo(description,myUrl)
                    }
                }
            }
            MenuItem {
                id: decompressSelect
                text: "Decompress"
                enabled: FileUtils.canBeDecompressed ( description )
                onTriggered: {
                    FileUtils.decompressFile( description )
                }
            }
            MenuItem {
                id: decompressTo
                text: "Decompress to..."
                enabled: decompressSelect.enabled
                signal receivedUrl(url myUrl)
                property bool mTriggered: false
                onTriggered: {
                    decompressDialog.show()
                    mTriggered = true
                }
                onReceivedUrl: {
                    if ( mTriggered ) {
                        mTriggered = false
                        console.log("decompressing to:" + myUrl)
                        FileUtils.decompressFileTo( description,myUrl )
                    }
                }
            }
        }
        MouseArea {
            id: mouseArea
            anchors.fill: parent
            //            onClicked: exampleItem.exampleUrl = url
            onClicked: {
                next.color = "black"
                middleBal.color = "black"
                buttonMenu.popup()
                //                console.log( description )
                console.log(index)
            }
            hoverEnabled: true
        }
    }
    Item {
        id: button
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.right:next.left
        implicitHeight: col.height
        height: implicitHeight
        width: buttonLabel.width + 20
        Column {
            spacing: 2
            id: col
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width
            Text {
                id: buttonLabel
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                anchors.rightMargin: 10
                text: name
                font.family: "Droid Sans"
                color: "#292929"
                font.pixelSize: 14
                renderType: Text.NativeRendering
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                styleColor: "white"
                style: Text.Raised
            }

        }
    }
    Rectangle {
        id: zBar
        height: 1
        color: "#909090"
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
    }
}
