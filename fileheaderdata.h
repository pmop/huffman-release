#ifndef FILEHEADERDATA_H
#define FILEHEADERDATA_H
#include <QDebug>
#include <QFile>
#include <QByteArray>
#include <QBuffer>
#include <QObject>
#include <QQueue>
#include <QBitArray>
#include <QDataStream>
#include "mtree.h"
enum HeaderStatus:quint8 {
    READY,
    FAIL
};
class FileHeaderData : public QObject {
    Q_OBJECT
     quint8 m_padding,m_treepadding,m_nameSize;
     HeaderStatus m_status;
     quint16 m_treeSize;
     QByteArray m_fileName;
     quint64 m_headerEndPosition, m_compressedDataSize;
     quint32 m_totalHeaderSize;
     QQueue<QBitArray> m_tree;
     void deserializeTree(QByteArray &, QQueue<QBitArray> &, qint16);
 public:
     void readFromFile(QFile &);
     FileHeaderData();
     static bool checkIfIsValid(QQueue<QBitArray> copy);
     qint8 padding() const;
     qint16 treeSize() const;
     qint16 nameSize() const;
     QByteArray fileName() const;
     quint64 headerEndPosition() const;
     void dbg();
     quint32 totalHeaderSize() const;
     void setPadding(const quint8 &padding);
     void setSerializedTreeSize(const quint16);
     void setFileName(const QByteArray &fileName);
     QByteArray getHeaderByteArray();
     quint64 getCompressedDataSize() const;

     quint8 getTreepadding() const;

     quint64 getHeaderEndPosition() const;

     QQueue<QBitArray>& getTree();

     HeaderStatus getStatus() const;

signals:
     void chunkBitArray(QBitArray chunk);
     void HeaderByteArray(QByteArray header);
     void headerStatus(HeaderStatus status);
     void startPositionForHuffmanData(quint32 position);
};

#endif /* end of include guard: FILEHEADERDATA_H */
