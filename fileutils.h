#ifndef FILEUTILS_H
#define FILEUTILS_H
#include "fileheaderdata.h"
#include <QObject>
#include <QFileInfo>
#include "compress.h"
#include "decompress.h"
#include <QUrl>
class FileUtils : public QObject
{
    Q_OBJECT
public:
    FileUtils();
    Q_INVOKABLE const QString fileName(const QString);
    Q_INVOKABLE const QString pathToFile(const QString url);
    Q_INVOKABLE void compressFile(const QString);
    Q_INVOKABLE void compressFileTo(const QString,const QUrl);
    Q_INVOKABLE void decompressFile(const QString);
    Q_INVOKABLE void decompressFileTo(const QString,const QUrl);
    Q_INVOKABLE bool canBeDecompressed(const QString);
    Q_INVOKABLE bool exists(const QUrl);
signals:
    Q_SIGNAL void errorChanged (const QString error);
    Q_SIGNAL void savedAtChanged (const QString path);
private slots:
    void forwardErrors(const QString err);
};

#endif // FILEUTILS_H
