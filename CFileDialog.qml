import QtQuick 2.0
import QtQuick.Dialogs 1.2
Item {
    id: ccFdialog
    property url mFileUrl
    property bool selectFolder: false
    signal receivedUrl(url mfileurl)
    visible: false
    function show() {
        mFileDialog.visible = true
    }
    FileDialog {
        id: mFileDialog
        visible: ccFdialog.visible
        selectExisting: false
        selectFolder: ccFdialog.selectFolder
        title: "Compress to..."
        onAccepted: {
            if ( FileUtils.exists(fileUrl) ) {
                ccFdialog.mFileUrl = fileUrl
                msgD.visible = true
            } else {
                ccFdialog.mFileUrl = fileUrl
                ccFdialog.receivedUrl(fileUrl)
            }
        }
    }
    MessageDialog {
        id: msgD
        visible: ccFdialog.visible
        title: "Overwrite?"
        text: ccFdialog.mFileUrl + " already exists. Overwrite ?"
        icon: StandardIcon.Question
        detailedText: "To replace a file means that its existing contents will be lost. " +
                      "The file that you are copying now will be copied over it instead."
        standardButtons: StandardButton.Yes | StandardButton.No
        Component.onCompleted: visible = false
        onYes: {
            ccFdialog.receivedUrl(ccFdialog.mFileUrl)
        }
    }
    onReceivedUrl: {
        console.log("(CFileDialog) " + mFileUrl)
    }
}

