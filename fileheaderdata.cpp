#include "fileheaderdata.h"
FileHeaderData::FileHeaderData() {
     m_headerEndPosition = 0;
     m_totalHeaderSize = 0;
     m_status = HeaderStatus::FAIL;
}
bool FileHeaderData::checkIfIsValid(QQueue<QBitArray> copy)
{
    QBitArray test = copy.takeLast ();
    if ( test.size () == 1 )
    {
        qDebug() << "Invalid tree. Possible invalid or corrupt file.";
        return false;
    } else {
        return true;
    }
}

quint32 FileHeaderData::totalHeaderSize() const {
    return m_totalHeaderSize;
}

void FileHeaderData::setPadding(const quint8 &padding) {
    m_padding = padding;
}

void FileHeaderData::setSerializedTreeSize(const quint16 value) {
    m_treeSize = value;
}
void FileHeaderData::setFileName(const QByteArray &fileName) {
    m_fileName = fileName;
    m_nameSize = static_cast<quint16>(fileName.size ());
}

QByteArray FileHeaderData::getHeaderByteArray() {
    quint16 rawPaddingAndTreeSizes = 0;
    rawPaddingAndTreeSizes ^= m_treeSize;//set first 2 bytes
    QByteArray header;
    header.reserve (3 + m_nameSize );
    QDataStream buffer(&header,QIODevice::WriteOnly);
    if ( m_padding & (1<<0) )
    {
        rawPaddingAndTreeSizes ^= (-1 ^ rawPaddingAndTreeSizes) & (1 << 13);
    }
    if ( m_padding & (1<<1) )
    {
        rawPaddingAndTreeSizes ^= (-1 ^ rawPaddingAndTreeSizes) & (1 << 14);
    }
    if ( m_padding & (1<<2) )
    {
        rawPaddingAndTreeSizes ^= (-1 ^ rawPaddingAndTreeSizes) & (1 << 15);
    }
    buffer << rawPaddingAndTreeSizes; //writes 2bytes
    buffer << m_nameSize;
    header.append (m_fileName);
    emit HeaderByteArray (header);
    return header;
}

quint64 FileHeaderData::getCompressedDataSize() const
{
    return m_compressedDataSize;
}

quint8 FileHeaderData::getTreepadding() const
{
    return m_treepadding;
}

quint64 FileHeaderData::getHeaderEndPosition() const
{
    return m_headerEndPosition;
}

QQueue<QBitArray> &FileHeaderData::getTree()
{
    return m_tree;
}

HeaderStatus FileHeaderData::getStatus() const
{
    return m_status;
}

void FileHeaderData::deserializeTree(QByteArray &serialized, QQueue<QBitArray> &deserialized, qint16 padding) {
    QBitArray bitArr = byteops::bitsFromBytes (serialized);
    int nSize = bitArr.size () - padding;
//    qDebug() << bitArr.size ();
//    qDebug() << "Tree" << bitArr;
    qint32 i = 0;
    while ( i < nSize ) {
        if ( !bitArr.at (i) ) {
            QBitArray signal(1,false);
            deserialized.enqueue (signal);
                i++;
        }
        else if ( bitArr.at (i) )
        {
            QBitArray signal(1,true);
            deserialized.enqueue (signal);
                i++;
            QBitArray value(8,false);
            for(quint8 c = 0; c < 8;++c) {
                if ( bitArr.at (i) ) {
                    value.setBit (c,true);
                }
                i++;
            }
            deserialized.enqueue (value);
        }
    }
}

void FileHeaderData::readFromFile(QFile &file) {
    QByteArray buffer = file.read(3);
    QDataStream stream(&buffer,QIODevice::ReadOnly);
    quint16 rawPaddingAndTreeSize;
    quint8 rawNameSize;
    stream >> rawPaddingAndTreeSize;
    stream >> rawNameSize;
    m_nameSize = rawNameSize;
    m_padding = 0;
    if ( rawPaddingAndTreeSize & (1<<13) )
    {
        m_padding += 1;
    }
    if ( rawPaddingAndTreeSize & (1<<14) )
    {
        m_padding += 2;
    }
    if ( rawPaddingAndTreeSize & (1<<15) )
    {
        m_padding += 4;
    }
    //set leftmost bits to 0 so we can directly read it as 16 bit unsigned integer
    rawPaddingAndTreeSize ^= (-0 ^ rawPaddingAndTreeSize) & (1 << 13);
    rawPaddingAndTreeSize ^= (-0 ^ rawPaddingAndTreeSize) & (1 << 14);
    rawPaddingAndTreeSize ^= (-0 ^ rawPaddingAndTreeSize) & (1 << 15);
    m_treeSize = rawPaddingAndTreeSize;
    file.seek(3);//first 3 bytes (padding + treesz + namesz)
    buffer = file.read(m_nameSize);
    m_fileName.append (buffer);
    file.seek(3 + m_nameSize);
    //m_treeSize are actually bites written so we have to convert it to bytes rounding it up

    quint16 padding = 0;
    while ( (m_treeSize + padding)%8 ) {
        padding++;
    }
    m_treepadding = padding;
    m_compressedDataSize =  (file.size ()*8) - ((m_nameSize * 8) + 24 + m_treeSize);
    buffer = file.read( (m_treeSize+padding)/8 );// read may fail if it finds an '\n'
    QByteArray serializedTree;
    serializedTree.append (buffer);
    deserializeTree (serializedTree,m_tree,(qint16)padding);
    m_headerEndPosition = 3 + m_nameSize + (m_treeSize+padding)/8;
    m_totalHeaderSize = 24 + (m_fileName.size()*8) + m_treeSize;
    emit startPositionForHuffmanData (m_headerEndPosition);
    if  ( m_tree.size () != 0 )
    {
        if ( FileHeaderData::checkIfIsValid (m_tree) ) {
            m_status = HeaderStatus::READY;
            emit headerStatus (HeaderStatus::READY);
        } else {
            emit headerStatus (HeaderStatus::FAIL);
        }
    } else {
        emit headerStatus (HeaderStatus::FAIL);
    }
}
qint8 FileHeaderData::padding() const
{
    return m_padding;
}
qint16 FileHeaderData::treeSize() const
{
    return m_treeSize;
}
qint16 FileHeaderData::nameSize() const
{
    return m_nameSize;
}
QByteArray FileHeaderData::fileName() const
{
    return m_fileName;
}
quint64 FileHeaderData::headerEndPosition() const
{
    return m_headerEndPosition;
}
void FileHeaderData::dbg()
{
    qDebug() <<"\n\nFileHeaderData dbg"<<padding ();
    qDebug() <<"\n\nPadding (bits) "<<padding ();
    qDebug() <<"Tree size (bits) "<<treeSize ();
    qDebug() <<"Name size (bytes) "<<nameSize ();
    qDebug() <<"Name: "<<fileName ();
    qDebug() <<"End position:"<<headerEndPosition ();
    qDebug() <<"Total header size:"<<m_totalHeaderSize;
    qDebug() <<"Compressed data size:" << m_compressedDataSize;
}
