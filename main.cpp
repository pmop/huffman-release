#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>
#include <QObject>
#include <QCoreApplication>
#include <QApplication>
#include <QCommandLineParser>
#include "compress.h"
#include "decompress.h"
#include "fileutils.h"
#include <QString>
/*
    TODO:
    Fix the append bug
    Fix segmentation fault in FileHeaderData
    Finish and polish graphic interface
*/
int main(int argc, char *argv[]) {
    QGuiApplication app(argc,argv);
    QGuiApplication::setApplicationName("huffman");
    QCommandLineParser cliParser;
    cliParser.setApplicationDescription( QStringLiteral("huffman") );
    cliParser.addHelpOption();
    cliParser.addPositionalArgument("<file>",
                                   QCoreApplication::translate("main","Source file to compress.") );

    cliParser.addPositionalArgument("<path>",
                                   QCoreApplication::translate("main","Destination for output file.") );

    QCommandLineOption compress("c",QCoreApplication::translate("main",
                                "huffman -c <file>. Compresses <file> and saves as <file>.huff.") );

    QCommandLineOption compressOutput("o",QCoreApplication::translate("main",
                                      "huffman -c <file> -o <name>. Compresses <file> and saves as <name>."));

    QCommandLineOption decompressAtPath("d",QCoreApplication::translate("main",
                                        "huffman <file> -d <path>. Decompresses <file> and saves at <path>."));

    QCommandLineOption goGui("gui",QCoreApplication::translate("main",
                            "huffman --gui. Runs huffman in gui mode."));

    QCommandLineOption dbg("dbg",QCoreApplication::translate("main",
                            "huffman --dbg. Debug."));


    cliParser.addOption(compress);
    cliParser.addOption(compressOutput);
    cliParser.addOption(decompressAtPath);
    cliParser.addOption(goGui);
    cliParser.addOption(dbg);
    cliParser.process(app);
    bool ccompress     = cliParser.isSet(compress),
            showGui       = cliParser.isSet(goGui),
            ocompress     = cliParser.isSet(compressOutput),
            ddecompress   = cliParser.isSet(decompressAtPath);
    if ( cliParser.isSet (dbg) )
    {
        QFile file( cliParser.positionalArguments ().at(0) );
        if ( file.open (QIODevice::ReadOnly) )
        {
            FileHeaderData h;
            h.readFromFile (file);
            qDebug() << ( h.getStatus () == HeaderStatus::READY ? "Ok!" : "Not Ok." );
            file.close ();
        }
        return 0;
    }
    if ( showGui ) {// [GUI]
//        QGuiApplication guiapp(argc,argv);
        QQmlApplicationEngine engine;
        FileUtils fileUtils;
        engine.rootContext ()->setContextProperty ("FileUtils",&fileUtils);
        engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
        return app.exec ();
    }
    if ( ccompress ) {
        if( ocompress ) {
            //Compress to path or file
            if ( cliParser.positionalArguments().count() == 2) {
                QString file = cliParser.positionalArguments().at(0);
                QString filename = cliParser.positionalArguments().at(1);
                Compress cfile;
                cfile.compressFile (file,filename);
            } else {
                qDebug() << "Invalid use. Check -h for help.";
            }
        }
        else {
            //            Compress file and save as filename
            QString file = cliParser.positionalArguments().at(0);
            Compress cfile;
            QString gambiarra;
            cfile.compressFile (file,gambiarra);
        }
        // c compress is not set, so we decompress
    } else {
        //if -d is set, decompress to "thing"
        if ( ddecompress ) {
            if ( cliParser.positionalArguments().count() == 2) {
                QString file = cliParser.positionalArguments().at(0);
                QString path = cliParser.positionalArguments().at(1);
                QDir test(path);
                if ( test.exists () ) {
                    Decompress srcfile;
                    srcfile.decompressFile (file,path);
                } else {
                    qDebug() << test.absolutePath () << "\n doesn't exists";
                }
            } else {
                qDebug() << "Invalid use. Check -h for help.";
            }
        // -d is not set, decompress to current path
        } else {
            if ( cliParser.positionalArguments().count() == 1 ) {
                QString file = cliParser.positionalArguments().at(0);
                Decompress srcfile;
                srcfile.decompressFile (file);
            }
            else {
                qDebug () << "Invalid use. huffman -h for help.";
            }
        }
    }
    return 0;
}
