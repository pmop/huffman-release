#ifndef ENCODE_H
#define ENCODE_H
#include <QObject>
#include <QDir>
#include <QUrl>
#include <QString>
#include <QFile>
#include <QByteArray>
#include <QIODevice>
#include <QDebug>
#include <QList>
#include <QObject>
#include <QDataStream>
#include <QVector>
#include "mtree.h"
#include "fileheaderdata.h"
#define LEFT 0
#define RIGHT 1
#define __INFO
#define GHEADERB 3
#define TREESIZEB 13

enum ErrorLevel:quint8 {
    NOTHING,
    WARNING,
    FATAL
};
class Compress
{
    MNode* m_root = nullptr;
    QVector<quint64> m_frequencies;
    QList<MNode *>* m_nodeKeyTable = nullptr;
    void countFrequency(const QByteArray& data);
    bool createNodeKeyTable();
    void createTree();
    bool encodeFile(QFile &file, QString path = "./");
public:
    Compress();
    ~Compress();
    bool readFile(QByteArray name, QString &saveAs);
    Q_INVOKABLE void compressFile(QString src,QString dest);
};
QString getFileName(QFile&,QString&);
QBitArray serializedTreeBits(QString* serializedTree);
#endif //ENCODE_H
