#include "compress.h"
Compress::Compress() {
    // QObject::connect (this, &Compress::error,
    //                   this, &Compress::parseError);
}
bool Compress::createNodeKeyTable() {
    if (m_frequencies.isEmpty ()) {
//        emit error ("Frequencies are empty or not counted",ErrorLevel::FATAL);
        return false;
    }
    m_nodeKeyTable = new QList <MNode *>;
    if ( m_nodeKeyTable ) {
        for(quint32 count = 0; count < 256; count++)
        {
            if(m_frequencies.at(count) != 0) {
                MNode* objectNode = new MNode( m_frequencies.at(count), count );
                (*m_nodeKeyTable) << objectNode;
            }
        }
    } else {
        qDebug() << "Couldn't allocate mem for keyTable";
        return false;
    }
    return true;
}
void Compress::countFrequency(const QByteArray& data) {
    if ( data.isNull () || data.isEmpty () ) {
//        emit error ("Null or empty file",ErrorLevel::FATAL);
        return;
    }
    QVector<quint64> frequencies(256,0);
    quint64 dataSize = data.size ();
    for ( quint64 i = 0; i < dataSize; ++i )
    {
        frequencies[ (uchar ( data.at(i) )) ] = frequencies.at(uchar( data.at(i) ))+1;
    }
    m_frequencies = frequencies;
}
void Compress::createTree() {
    if ( m_nodeKeyTable ) {
        qSort(m_nodeKeyTable->begin(),m_nodeKeyTable->end(),compNode);
        while( m_nodeKeyTable->size() > 1 ) {
            MNode* node1 = m_nodeKeyTable->takeFirst();
            MNode* node2 = m_nodeKeyTable->takeFirst();
            MNode* internalNode = sum(node1,node2);
            m_nodeKeyTable->prepend(internalNode);
            qSort(m_nodeKeyTable->begin(),m_nodeKeyTable->end(),compNode);
        }
        m_root = m_nodeKeyTable->takeFirst();
    } else {
        qDebug() << "Null nodeKeyTable";
    }
}

bool Compress::encodeFile(QFile &file,QString path) {
    quint64 totalSize = 0;
    QVector<QBitArray> codeTable(256);
    QFileInfo info(file);
    QByteArray fileName;
    fileName.append (info.fileName () );
    qDebug() << fileName;
    //Get code table and total encoded file Size
    m_root->generateCodeTable(m_root,codeTable,&totalSize);
    if (fileName.size () > 256) {
        qDebug() << "File name is too big";
        return false;
    }
    QQueue<QBitArray> treeStream;
    quint64 totalSerializedTreeSize = 0;
    //Get serialized tree (it's a bit array and may not be % 8)
    m_root->serializeTree (m_root,&treeStream,&totalSerializedTreeSize);
    qDebug() << "Total serialized tree size:" << totalSerializedTreeSize;
    //We only have 13 bits to save it's size so we can't go past it
    if (totalSerializedTreeSize > 8192) {
        qDebug() << "Tree size type overflow";
        return false;
    }
    QBitArray compressedData;
    ushort padding = 0;
    while ( ( totalSize+padding+totalSerializedTreeSize )%8 ) {
        padding++;
    }
    /*We're allocating not only space for encoded data but also for serialized tree
     *because it both may not be %8
    */
    compressedData.resize( totalSerializedTreeSize+totalSize+padding );
    compressedData.fill (0);

    //Making up header
    FileHeaderData header;
    header.setPadding (padding);
    header.setFileName (fileName);
    header.setSerializedTreeSize (totalSerializedTreeSize);

    quint64 aux = 0;
    while ( !treeStream.isEmpty () ) {
        QBitArray item = treeStream.dequeue ();
        for(qint32 i = 0; i < item.size ();++i )
        {
            if ( item.at (i) )
            {
                compressedData.setBit (aux,true);
            }
            aux++;
        }
    }
    QByteArray data;
    QFile rfile(file.fileName ());
    if ( rfile.open(QIODevice::ReadOnly) ) {
         // read everything into data, we'll buffer it later
        data = rfile.readAll();
    } else {
        return false;
    }
    quint64 dataSize = static_cast<quint64>(rfile.size ());
    qDebug() << "Source size:" << dataSize;
    for (quint64 i = 0;i < dataSize;++i) {
        QBitArray code = codeTable.at( quint8(data.at (i)) ); // get codification for that byte
        for(qint32 c = 0; c < code.size (); ++c) {
            if ( code.at (c) ) {
                compressedData.setBit (aux,true);
            }
            aux++;
        }
    }
    qDebug() << "Header size:" << header.getHeaderByteArray ().size ();
    qDebug() << "Encoded file size:" << compressedData.size()/8;

    QString fileInfo = getFileName(file,path);
    qDebug() << fileInfo;
    QFile dFile( fileInfo );
    if ( dFile.open (QIODevice::WriteOnly) ) {
        QByteArray fullyEncodedFile;
        fullyEncodedFile.append (header.getHeaderByteArray ());
        fullyEncodedFile.append ( byteops::bytesFromBits (compressedData) );
        dFile.write (fullyEncodedFile);
    } else {
        return false;
    }
    qDebug() << "Final file size:" << dFile.size ();
    dFile.close ();
    return true;
}

Compress::~Compress()
{
    if (m_nodeKeyTable)
    {
        delete m_nodeKeyTable;
    }
    m_frequencies.clear ();
}

bool Compress::readFile(QByteArray name,QString& saveAs) {
    QFile file(name);
    if ( file.exists () ) {
        if ( file.open (QIODevice::ReadOnly) ) {
            QByteArray data = file.readAll ();
            countFrequency (data);
            createNodeKeyTable ();
            createTree ();
            encodeFile (file,saveAs);//remove from there afterwards
            file.close ();
        } else {
            //            emit error(file.errorString (), ErrorLevel::FATAL);
            qDebug() << file.errorString ();
            return false;
        }
    } else {
        //        emit error(file.errorString (), ErrorLevel::FATAL);
            qDebug() << "Error: File not found";
            return false;
    }
    return true;
}

void Compress::compressFile(QString src, QString dest)
{
    QByteArray source;
    source.append (src);
    if ( !(dest.isNull () || dest.isEmpty ()) ) {

    }
    readFile (source,dest);
}

QString getFileName(QFile &file, QString &dst)
{
    QFileInfo fileInfo(file);
    QString sfile;
    if ( dst.isEmpty () )
    {
        //no destination given, save at current directory
        QFileInfo fdst;
        fdst.setFile ("./" + fileInfo.baseName () + ".huff");
        sfile = fdst.absoluteFilePath ();
    }
    else {
        //destination was given
        QFileInfo fdst(dst);
        if ( fdst.fileName ().isEmpty () )
        {
            //only destination given
            fdst.setFile ( fdst.absolutePath () + "//" + fileInfo.baseName () + ".huff" );
            sfile = fdst.absoluteFilePath ();
        } else {
            if ( fdst.path ().isEmpty () )
            {
                //only file name was given
                fdst.setFile ("./" + fdst.baseName () + ".huff");
                sfile = fdst.absoluteFilePath ();
            } else {
                //file name and destination given
                fdst.setFile ( fdst.absolutePath () + "//" + fdst.baseName () + ".huff");
                sfile = fdst.absoluteFilePath ();
            }
        }
    }
    return sfile;
}
