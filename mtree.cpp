#include "mtree.h"
MNode::MNode(const quint64 weight,const int16_t key,MNode* l,MNode*r)
{
    lc = l;
    rc = r;
    mParent = nullptr;
    mWeight = weight;
    mKey = key;
}
MNode::MNode(const quint64 weight,const int16_t key)
{
    lc = nullptr;
    rc = nullptr;
    mParent = nullptr;
    mWeight = weight;
    mKey = key;
}

void MNode::setParent(MNode *parent)
{
    mParent = parent;
}

MNode* &MNode::getRc()
{
    return rc;
}

void MNode::setRc(MNode *value)
{
    rc = value;
}

quint64 MNode::weight() const
{
    return mWeight;
}

void MNode::setWeight(const quint64 weight)
{
    mWeight = weight;
}

int16_t MNode::key() const
{
    return mKey;
}

void MNode::setKey(const int16_t key)
{
    mKey = key;
}
MNode* &MNode::getLc()
{
    return lc;
}
void MNode::dbg(MNode* &node)
{
    if ( node != nullptr )
    {
        std::stringstream str;
        str << node->weight() << ":" << ((node->key() == -1) ? ('\\') : static_cast<char>(node->key()));
        if( node->key() != -1 )
        {
            qDebug() << "Leaf";
        }
        qDebug() << str.str().c_str();
        if( node->getLc() != nullptr )
        {
            qDebug() << "Left of" << str.str().c_str();
            dbg( node->lc );
        }
        if( node->getRc() )
        {
            qDebug() << "Right of" << str.str().c_str();
            dbg( node->rc );
        }
    }
    else
    {
        qDebug() << "Null node";
    }
}

//Old
/*
        New plan is use QBitArray and store 0 if node and 1 then 8bits if key.
        New new plan use QByteArray and bitshifting operators to do it.
*/
void MNode::serializeTree(MNode *node, QByteArray *serialStream,bool keyAsInt) {
    if ( (node != nullptr) && (serialStream != nullptr) )
    {
        if( node->key() != -1 )
        {
            if ( keyAsInt )
            {
                (*serialStream) += uint( uchar( node->key() ) );
            }
            else
            {
                if ( node->key() == uchar ('*') ) // workaround
                {
                    (*serialStream) += node->key();
                    (*serialStream) += '!';
                } else
                {
                    (*serialStream) += node->key();
                }
            }
        }
        else
        {
            (*serialStream) += "*";
        }
        if( node->getLc() )
        {
            serializeTree( node->lc,serialStream );
        }
        if( node->getRc() )
        {
            serializeTree( node->rc,serialStream );
        }
    }
    else
    {
        qDebug() << "Null node or serialStream";
    }
}

void MNode::serializeTree(MNode *node, QQueue<QBitArray> *stream, quint64 *totalSize) {
    if ( (node != nullptr) && (stream != nullptr) )
    {
        if( node->key() != -1 ) {
            QByteArray keyval; keyval.append ( quint8(node->key ()) );
            QBitArray signal(1,true);
            QBitArray byte = byteops::bitsFromBytes (keyval);
            stream->enqueue (signal);
            stream->enqueue (byte);
            if (totalSize) {
                (*totalSize) += 9;
            }
        }
        else
        {
            QBitArray signal(1,false);
            stream->enqueue (signal);
            if (totalSize) {

                (*totalSize) += 1;
            }
        }
        if( node->getLc() )
        {
            serializeTree( node->lc,stream,totalSize);
        }
        if( node->getRc() )
        {
            serializeTree( node->rc,stream,totalSize);
        }
    }
    else
    {
        qDebug() << "Null node or serialStream";
    }
}

bool MNode::hasParent()
{
    return mParent != nullptr;
}

void MNode::generateCodeTable(MNode* node, QVector<QBitArray>& codes, quint64 *totalSize) {
    if ( node->lc )
    {
        if ( node->lc->mParent )
        {
            node->lc->code += node->lc->mParent->code;
            node->lc->code += '0';
        }
        else
        {
            node->lc->code += '0';
        }
        generateCodeTable(node->lc,codes,totalSize);
    }
    if ( node->rc )
    {
        if ( node->rc->mParent )
        {
            node->rc->code += node->rc->mParent->code;
            node->rc->code += '1';
        }
        else
        {
            node->rc->code += '1';
        }
        generateCodeTable(node->rc,codes,totalSize);
    }
    if ( node->isLeaf() )
    {
        QBitArray codeBits(node->code.size(),false);
        for (quint16 i = 0;i < node->code.size();++i)
        {
            if ( node->code.at( i ) == '1' )
            {
                codeBits.setBit(i,true);
            }
        }
        codes[ static_cast<int>(node->key ()) ] = codeBits;
        (*totalSize) += node->weight () * codeBits.count ();
        node->code.clear ();
    }
}
MNode::~MNode()
{
    if ( lc )
    {
        delete lc;
    }
    if ( rc )
    {
        delete rc;
    }
    code.clear();
    mParent = nullptr;
}

void MNode::setLc(MNode* &value)
{
    lc = value;
}
bool MNode::isLeaf()
{
    return (lc == nullptr) && (rc == nullptr);
}

MNode* sum(MNode* node1,MNode* node2)
{
    MNode* parentNode = new MNode( ( node1->weight() + node2->weight() ), -1, node1, node2);
    node1->setParent( parentNode );
    node2->setParent( parentNode );
    return parentNode;
}
bool compNode(MNode* &a,MNode* &b)
{
    return a->weight() < b->weight();
}
QBitArray byteops::bitsFromBytes(const QByteArray data) {
    QBitArray bits;
    bits.resize (data.size () * 8);
    bits.fill (false);
    for(int i=0; i<data.count(); ++i) {
        for(int b=0; b<8; b++) {
            bits.setBit( i*8+b, data.at(i)&(1<<(7-b)) );
        }
    };
    return bits;
}
QByteArray byteops::bytesFromBits(const QBitArray data) {
    QByteArray byteData;
    byteData.resize( ((data.count()/8) > 0) ? (data.count()/8) : 1 );
    byteData.fill(0);
    for(int b=0; b<data.count(); ++b) {
        byteData[b/8] = (byteData.at(b/8) | ((data[b] ? 1 : 0)<<(7-(b%8))));
    }
    return byteData;
}
