#-------------------------------------------------
#
# Project created by QtCreator 2015-05-06T12:57:39
#
#-------------------------------------------------

TEMPLATE = app
QT        += qml quick widgets
CONFIG += c++11

TARGET = huffman

SOURCES +=\
    main.cpp \
    compress.cpp \
    decompress.cpp \
    fileheaderdata.cpp \
    mtree.cpp \
    huffmandata.cpp \
    fileutils.cpp

HEADERS  += \
    mtree.h \
    compress.h \
    decompress.h \
    fileheaderdata.h \
    huffmandata.h \
    fileutils.h

DISTFILES +=

RESOURCES += qml.qrc

QML_IMPORT_PATH =
include (deployment.pri)
