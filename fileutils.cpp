#include "fileutils.h"

FileUtils::FileUtils()
{

}

const QString FileUtils::fileName(const QString url)
{
    QUrl murl(url);
    QFileInfo info(murl.toLocalFile ());
    return info.fileName ();
}

const QString FileUtils::pathToFile(const QString url)
{
    QUrl murl(url);
    QFileInfo info(murl.toLocalFile ());
    return info.absoluteFilePath ();
}

void FileUtils::compressFile(const QString pathToFile)
{
    QFileInfo info(pathToFile);
    Compress cfile;
    cfile.compressFile (pathToFile, info.absolutePath () + "/" );
    //handle possible errors here;
    //handle "file already exists cases"
    emit savedAtChanged( info.absoluteFilePath () );
}

void FileUtils::compressFileTo(const QString pathToFile, const QUrl as)
{
    Compress cfile;
    cfile.compressFile (pathToFile, as.toLocalFile () );
    //handle possible errors here;
    //handle "file already exists cases"
    emit savedAtChanged(as.toLocalFile ());
}

void FileUtils::decompressFile(const QString src)
{
    QFileInfo info(src);
    Decompress dfile;
    QObject::connect (&dfile,&Decompress::error,
                      this,&FileUtils::forwardErrors);

    if ( info.fileName ().contains (".huff") )
    {
        qDebug() << "Is .huff";
        dfile.decompressFile ( src,info.absolutePath () );
        emit savedAtChanged ( src );
    }
}

void FileUtils::decompressFileTo(const QString src, const QUrl dst)
{

    Decompress dfile;
    QObject::connect (&dfile,&Decompress::error,
                      this,&FileUtils::forwardErrors);
    QFileInfo info(src);

    dfile.decompressFile ( src, dst.toLocalFile () );
    if ( info.fileName ().contains (".huff") )
    {
        qDebug() << "Is .huff";
        dfile.decompressFile ( src,info.absolutePath () );
        emit savedAtChanged ( src );
    }
}


bool FileUtils::canBeDecompressed(const QString pathTofile)
{
    QFile file(pathTofile);
    if ( file.open (QIODevice::ReadOnly) )
    {
        FileHeaderData h;
        h.readFromFile (file);
        file.close ();
        if(h.getStatus () == HeaderStatus::READY) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

bool FileUtils::exists(const QUrl pathToFile)
{
    QFileInfo info( pathToFile.toLocalFile () );
    if ( info.isDir() ) {
        //folder ?
        return false;
    } else {
        if ( info.exists () ) {
            qDebug() << "(FileUtils) Already exists:" << info.absoluteFilePath ();
            return true;
        } else {
            return false;
        }
    }
}

void FileUtils::forwardErrors(const QString err)
{
    emit errorChanged (err);
}
