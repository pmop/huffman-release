#ifndef HUFFMANDATA_H
#define HUFFMANDATA_H
#include <QFile>
#include <QDebug>
#include <QObject>
#include "mtree.h"
#include "fileheaderdata.h"
enum FileStatus : quint8 {
    FREADY,
    NOTREADY,
    REACHEDEOF,
    FAILURE
};

class HuffmanData : public QObject {
    Q_OBJECT
    quint64 m_iterator;
    MNode* m_tree = nullptr;
    FileStatus m_fileStatus;
    FileHeaderData *m_headerData;
    QQueue<QBitArray> m_serializedTree;
    QBitArray *m_bitChunk;
    QByteArray m_decodedBytes;
public:
    MNode *buildNode();
    HuffmanData();
    void setup(FileHeaderData*);
    ~HuffmanData();
    void decode();
    void dbg();
signals:
    void decodedBytes(QByteArray bytes);
    void requestBytesToWrite(quint64 atPosition);
    void requestBitChunk();
    void error(const QString);
public slots:
    void receiveBitChunk(QBitArray *chunk,FileStatus status);
    void decodeTheseBytes(QByteArray bytes, FileStatus status );
};
#endif // HUFFMANDATA_H
