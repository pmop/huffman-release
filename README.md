# Huffman
![Image of GUi](https://github.com/pmop/huffman-release/blob/master/raw/huffmangui.png)

Data compression using huffman compression algorithm.

### Requirements
- `qmake`
- `Qt 5.5 libs`

### Building
```
git clone "github.com/pmop/huffman/"
cd ./huffman
qmake
make
```
### Running and usage
Usage:


![Image of Term](https://github.com/pmop/huffman-release/blob/master/raw/term.png)
- `./huffman [options] <file> <path>`

# About the project
This project uses huffman coding algorithm as approach for loss-less data compression. We are also using C++ and the Qt framework.
Algorithm and language/framework choices are more a result of class requirements than previous planning/modeling. It is perfectly possible to use another language and another framework, huffman coding is a pretty straightforward algorithm and easy to implement; usually, it's implemented in C.
One advantage of using C++ over C is that you can implement it all much faster than in C and it's generally better to handle errors than with C, thought, if you not take care, your code may become pretty hairy.
Qt framework eases it futher. If you're using C++, it's easier (some will find less painful) to implement gui with Qt. With Qt containers and classes, you'll be able to avoid pointers and develop stable programs with less hair in your code. Of course, you may want to use boost or STL because of that speed, but still, Qt is pretty good at managing memory and, if take care of what you're doing your program will be pretty fast too (and people won't be scared of reading your code).
